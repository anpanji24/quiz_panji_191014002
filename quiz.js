const express = require('express')
const app = express()
const port = 5050

const parser = require("body-parser")

// parse application/json
app.use(parser.json())

const jsonBody=parser.json()
app.get('/', (req, res) => {
    res.send('Belajar Web 3')
});

app.post('/nama', jsonBody, (req, res) => {
    res.json({
        nama : 'Panji Alpian Nurfadillah',
        email : 'panji.alpian007@gmail.com',
        no : '08995823903',
    });
});

app.get('/nama', (req, res) => {
    var nma=["nama"];
    res.send(nma);
});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
});